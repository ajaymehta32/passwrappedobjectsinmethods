package ajaymehta.passwrappedobjectsinmethods;

import android.content.Intent;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

// here is 1 disadvange ..when you pass any wrapper class in an Object type...you lose all the method that wrapper class has for opertions


public class Program1 {

// Object behaves as a generic type ...it doesnt limit ..you .. Integer , String or any other type of Collection ..
    public static void display1(Object x) { // Object is a parent class of all ..that can hold any object type .n can simply print it ..


        // Object is just good for prinint ..otherwise
        // x has few methods of Object class like  x.equuals() , x.hasCode() etc ..so not much of use ... but can be helpful in prinitng..
        System.out.println(x);  //

        //  you dont have to cast or ...String.valueOf ... Object class just hold value n print the actual value comming..
    /*    System.out.println(String.valueOf(x));
        System.out.println( (Integer) x);
*/
    }


    public static void main(String args[]) {


        display1("Hello");  //String ..
        display1(3);        // Integer type ( objects)  wrapper classes..
        display1(3.4f);     // Float
        display1('A');    // Character

    }


}

package ajaymehta.passwrappedobjectsinmethods;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program2 {

    public static void display1(Integer x) {


        //see here we getting object of particular type ....Integer type ..so  now x can have methods of Integer  class
      //  x.compareTo()
      //  x.doubleValue() .. n many more ...
        // so pass a particular type has a lot of advantages..
        // passing Object type is good for ..just printing ..as much as i think so far



        System.out.println(x);

    }



    public static void main(String args[]) {



        display1(3);        // now display will take only Integer type..
     //   display1(3.3);        // you cant pass Float object to hold in Integer type of Classl


    }
}
